﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MoreLinq.Extensions;
using Reversi.Core.Engine.Extensions;

namespace Reversi.Core.Engine
{
    public sealed class Board : IBoard
    {
        private const double SlopeTolerance = 0.01;
        private const int BoardSize = 8;
        private const string BoardCoordinates = "abcdefgh";

        private readonly Lazy<IDictionary<DiskState, IEnumerable<int>>> _availableMoves;
        private DiskState[] _cells;

        public static Cell CellFromCoordinates(string coordinates, IBoard board = null)
        {
            coordinates = FixCoordinates(coordinates);
            ThrowIfInvalidCoordinates(coordinates);
            var first = coordinates.ToCharArray()[0];
            var last = coordinates.ToCharArray()[1];
            var x = BoardCoordinates.IndexOf(char.ToLower(first));
            var y = last - '0' - 1;

            return board != null
                ? new Cell(x, y, board[x, y])
                : new Cell(x, y);
        }

        public static Cell CellFromIndex(int index, IBoard board = null)
        {
            var x = index % BoardSize;
            var y = index / BoardSize;

            return board != null 
                ? new Cell(x, y, board[x, y]) 
                : new Cell(x, y);
        }

        public static Cell CellFromXy(int x, int y, IBoard board) => new Cell(x, y, board[x, y]);

        public static int IndexFromCoordinates(string coordinates)
        {
            var cell = CellFromCoordinates(coordinates);
            return cell.Index;
        }

        public static int IndexFromXy(int x, int y)
        {
            var cell = new Cell(x, y);
            return cell.Index;
        }

        public static string CoordinatesFromXy(int x, int y)
        {
            if (x < 0 || x >= BoardSize || y < 0 || y >= BoardSize)
                return string.Empty;
            var first = BoardCoordinates[x];
            var last = y + 1;

            return $"{first}{last}".ToUpper();
        }

        public static string CoordinatesFromIndex(int i)
        {
            if (i > BoardSize * BoardSize)
                return string.Empty;

            var cell = CellFromIndex(i, null);
            return CoordinatesFromXy(cell.X, cell.Y);
        }

        public Board()
        {
            Initialize();
            _availableMoves = new Lazy<IDictionary<DiskState, IEnumerable<int>>>(BuildAvailableMovesNew);
        }

        public Board(IEnumerable<DiskState> board)
        {
            _cells = board.ToArray();
            _availableMoves = new Lazy<IDictionary<DiskState, IEnumerable<int>>>(BuildAvailableMovesNew);
        }

        public DiskState this[string coordinates] => Get(coordinates);

        public DiskState this[char x, int y] => Get($"{x}{y}");

        public DiskState this[int y, char x] => this[x, y];

        DiskState IBoard.this[int x, int y] => Get(x, y);

        public IDictionary<DiskState, IEnumerable<int>> AvailableMoves => _availableMoves.Value;

        public Score Score
        {
            get
            {
                return new Score(_cells.Count(c => c == DiskState.Black), _cells.Count(c => c == DiskState.White));
            }
        }

        public IEnumerator<DiskState> GetEnumerator()
        {
            return _cells
                .AsEnumerable()
                .GetEnumerator();
        }

        public bool HasAnyMoves => HasMoves.All(i => i.Value);

        public IDictionary<DiskState, bool> HasMoves => AvailableMoves
            .Any()
            ? AvailableMoves.ToDictionary(i => i.Key, j => j.Value.Any())
            : new Dictionary<DiskState, bool>
            {
                { DiskState.Black, false },
                { DiskState.White, false }
            };

        public bool IsValidMove(string coordinates, DiskState disk)
        {
            var index = IndexFromCoordinates(coordinates);
            return IsValidMove(index, disk);
        }

        public async Task<bool> IsValidMoveAsync(string coordinates, DiskState disk, CancellationToken cancellationToken = default(CancellationToken))
        {
            var index = IndexFromCoordinates(coordinates);
            return await IsValidMoveAsync(index, disk, cancellationToken);
        }

        public bool IsValidMove(int index, DiskState disk)
        {
            return AvailableMoves[disk].Contains(index);
        }

        public Task<bool> IsValidMoveAsync(int index, DiskState disk, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            return Task.FromResult(AvailableMoves[disk].Contains(index));
        }

        public IBoard Move(string coordinates, DiskState disk)
        {
            var cell = CellFromCoordinates(coordinates);
            return MoveImpl(cell.X, cell.Y, disk);
        }

        public async Task<IBoard> MoveAsync(string coordinates, DiskState disk, CancellationToken cancellationToken = default(CancellationToken))
        {
            var cell = CellFromCoordinates(coordinates);
            return await MoveImplAsync(cell.X, cell.Y, disk, cancellationToken);
        }

        public IBoard Move(char x, int y, DiskState disk)
        {
            var coordinates = $"{x}{y}";
            var cell = CellFromCoordinates(coordinates);
            return MoveImpl(cell.X, cell.Y, disk);
        }

        public async Task<IBoard> MoveAsync(char x, int y, DiskState disk, CancellationToken cancellationToken = default(CancellationToken))
        {
            var coordinates = $"{x}{y}";
            var cell = CellFromCoordinates(coordinates);
            return await MoveImplAsync(cell.X, cell.Y, disk, cancellationToken);
        }

        private void Initialize()
        {
            _cells = new DiskState[BoardSize * BoardSize];
            Set("D4", DiskState.White);
            Set("E4", DiskState.Black);
            Set("D5", DiskState.Black);
            Set("E5", DiskState.White);
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private IBoard MoveImpl(int x, int y, DiskState disk)
        {
            var index = IndexFromXy(x, y);
            if (!IsValidMove(index, disk))
                throw new InvalidMoveException(this, index);

            var incrementors = GetIncrementors(x, y);
            var flipIndexes = incrementors
                .SelectMany(inc =>
            {
                var localFlipIndexes = new List<int>();
                var moveX = x;
                var moveY = y;
                var check = disk;
                do
                {
                    moveX += inc.Item1;
                    moveY += inc.Item2;
                    if (moveX < 0 || moveX >= BoardSize ||
                        moveY < 0 || moveY >= BoardSize)
                        break;

                    var lookIndex = IndexFromXy(moveX, moveY);
                    check = _cells[lookIndex];
                    if (check == disk.Flip())
                    {
                        localFlipIndexes.Add(lookIndex);
                    }

                } while (check == disk.Flip());

                return check == disk && localFlipIndexes.Any()
                    ? localFlipIndexes 
                    : new List<int>();
            }).ToList();

            if (!flipIndexes.Any())
                throw new InvalidMoveException(this, index);

            var build = this.ToArray();
            build[index] = disk;
            flipIndexes.ForEach(i => build[i] = build[i].Flip());
            return new Board(build);
        }

        private Task<IBoard> MoveImplAsync(int x, int y, DiskState disk, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var index = IndexFromXy(x, y);
            if (!IsValidMove(index, disk))
                throw new InvalidMoveException(this, index);

            var incrementors = GetIncrementors(x, y);
            var flipIndexes = incrementors
                .SelectMany(inc =>
            {
                var localFlipIndexes = new List<int>();
                var moveX = x;
                var moveY = y;
                var check = disk;
                do
                {
                    moveX += inc.Item1;
                    moveY += inc.Item2;
                    if (moveX < 0 || moveX >= BoardSize ||
                        moveY < 0 || moveY >= BoardSize)
                        break;

                    var lookIndex = IndexFromXy(moveX, moveY);
                    check = _cells[lookIndex];
                    if (check == disk.Flip())
                    {
                        localFlipIndexes.Add(lookIndex);
                    }

                } while (check == disk.Flip());

                return check == disk && localFlipIndexes.Any()
                    ? localFlipIndexes
                    : new List<int>();
            }).ToList();

            if (!flipIndexes.Any())
                throw new InvalidMoveException(this, index);

            var build = this.ToArray();
            build[index] = disk;
            flipIndexes.ForEach(i => build[i] = build[i].Flip());
            var board = new Board(build) as IBoard;

            return Task.FromResult(board);
        }

        private static IEnumerable<Tuple<int,int>> GetIncrementors(int x, int y)
        {
            return from xInc in Enumerable.Range(-1, 3)
                   from yInc in Enumerable.Range(-1, 3)
                   where !(xInc == 0 && yInc == 0) && 
                         x + xInc >= 0 && x + xInc < BoardSize && 
                         y + yInc >= 0 && y + yInc < BoardSize
                   select new Tuple<int, int>(xInc, yInc);
        }

        private bool HasNeighbor(int index, DiskState disk)
        {
            if (index < 0)
                return false;

            var cell = CellFromIndex(index, this);
            var incrementors = GetIncrementors(cell.X, cell.Y);
            var hasNeighbor = incrementors.Any(i => disk == Get(cell.X + i.Item1, cell.Y + i.Item2));
            return hasNeighbor;
        }

        private static Tuple<int, int> GetIncrementor(int x1, int y1, int x2, int y2)
        {
            var incX = 0;
            int incY;

            var deltaY = y2 - y1;
            var deltaX = x2 - x1;

            if (deltaX == 0)
                incY = deltaY > 0 ? 1 : -1;
            else
            {
                var slope = (float) deltaY / deltaX;
                if (Math.Abs(slope - 1.0) > SlopeTolerance)
                {
                    return null;
                }

                incX = deltaX > 0 ? 1 : -1;
                incY = deltaY > 0 ? 1 : -1;
            }

            if (deltaY == 0)
                incX = deltaX > 0 ? 1 : -1;

            return new Tuple<int, int>(incX, incY);
        }

        private IEnumerable<DiskState> DisksBetween(string coordinatesStart, string coordinatesEnd)
        {
            var cellStart = CellFromCoordinates(coordinatesStart);
            var cellEnd = CellFromCoordinates(coordinatesEnd);

            var incrementor = GetIncrementor(cellStart.X, cellStart.Y, cellEnd.X, cellEnd.Y);
            if (incrementor == null)
                yield break;

            var x = cellStart.X + incrementor.Item1;
            var y = cellStart.Y + incrementor.Item2;

            while (x != cellEnd.X && y != cellEnd.Y)
            {
                yield return Get(x, y);
                x += incrementor.Item1;
                y += incrementor.Item2;
            }
        }

        private Tuple<DiskState, IEnumerable<int>> CollectMovesFor(DiskState disk)
        {
            var emptyPotentials = _cells
                .Select((d, i) => d == DiskState.Empty ? i : -1)
                .Where(i => HasNeighbor(i, disk.Flip()))
                .Select(CoordinatesFromIndex)
                .ToList();

            var check = disk;
            var targets = _cells
                .Select((d, i) => d == check ? i : -1)
                .ExcludeIf(i => i < 0)
                .Select(CoordinatesFromIndex);

            var crossMoves =
                from p in emptyPotentials
                from t in targets
                select new {p, t};

            var collection = new List<string>();
            foreach (var move in crossMoves)
            {
                var disks = DisksBetween(move.p, move.t);
                if (disks.Any())
                    collection.Add(move.p);
            }

            //var moves =
            //    from t in targets
            //    where DisksBetween(p, t).All(d => d == disk)
            //    select p).Distinct();

            return new Tuple<DiskState, IEnumerable<int>>(disk, collection.Select(IndexFromCoordinates));
        }

        private IDictionary<DiskState, IEnumerable<int>> BuildAvailableMovesNew()
        {
            var diskStates = new[] {DiskState.Black, DiskState.White};

            var moves = diskStates
                .Select(CollectMovesFor)
                .ToDictionary(i => i.Item1, j => j.Item2);
            return moves;
        }

        private IDictionary<DiskState, IEnumerable<int>> BuildAvailableMoves()
        {
            var diskStates = new[] { DiskState.Black, DiskState.White };
            var moves = diskStates
                .AsParallel()
                .Select(s =>
                {
                    var diskIndicies = _cells.Select((ds, i) => ds == s ? i : -1).ExcludeIf(i => i < 0);

                    var indexMoves = diskIndicies
                        .AsParallel()
                        .SelectMany(i =>
                        {
                            var cell = CellFromIndex(i, this);
                            int x;
                            int y;

                            var check = s;
                            var incrementors = GetIncrementors(cell.X, cell.Y);
                            var coordinates = incrementors
                                .Select(inc =>
                            {
                                cell = CellFromIndex(i, this);
                                x = cell.X;
                                y = cell.Y;

                                var oppositeDiskCount = 0;
                                do
                                {
                                    x += inc.Item1;
                                    y += inc.Item2;
                                    if (x < 0 || x >= BoardSize || y < 0 || y >= BoardSize)
                                        break;
                                    check = Get(x, y);

                                    if (check == s.Flip())
                                        oppositeDiskCount++;

                                } while (check != DiskState.Empty &&
                                         check == s.Flip());

                                if (check != DiskState.Empty || oppositeDiskCount <= 0)
                                    return -1;

                                var moveCell = new Cell(x, y, check);
                                return moveCell.Index;
                            }).ExcludeIf(idx => idx < 0);

                            return coordinates;
                        })
                        .Distinct()
                        .OrderBy(c => c)
                        .AsEnumerable()
                        .ToList();

                    return new Tuple<DiskState, IEnumerable<int>>(s, indexMoves);
                })
                .Where(j => j.Item2.Any())
                .ToDictionary(i => i.Item1, j => j.Item2);

            return moves;
        }

        private static void ThrowIfInvalidCoordinates(string coordinates)
        {
            // coordinates invalid size
            if (coordinates.Length != 2)
                throw new InvalidCoordinatesException(coordinates);

            var first = coordinates.ToCharArray()[0];
            var last = coordinates.ToCharArray()[1];

            // coordinates are both letters or both numbers
            if (char.IsLetter(first) && char.IsLetter(last) || char.IsNumber(first) && char.IsNumber(last))
                throw new InvalidCoordinatesException(coordinates);

            // First character is lowercase
            first = char.ToLower(first);

            // Coordinates don't adhere to board characters
            var lastValue = int.Parse(last.ToString());
            if (!BoardCoordinates.Contains(first) || lastValue < 1 || lastValue > 8)
                throw new InvalidCoordinatesException(coordinates);
        }

        private static string FixCoordinates(string coordinates)
        {
            var first = coordinates.ToCharArray()[0];
            var last = coordinates.ToCharArray()[1];

            if (!char.IsLetter(last) || !char.IsNumber(first))
                return new string(new[] { first, last });

            return new string(new[] { last, first });
        }

        private DiskState Get(string coordinates)
        {
            var index = IndexFromCoordinates(coordinates);
            return _cells[index];
        }

        private DiskState Get(int x, int y)
        {
            var index = IndexFromXy(x, y);
            return _cells[index];
        }

        private void Set(string coordinates, DiskState state)
        {
            var index = IndexFromCoordinates(coordinates);
            Set(index, state);
        }

        private void Set(int index, DiskState state)
        {
            _cells[index] = state;
        }

        public struct Cell
        {
            public int X { get; set; }
            public int Y { get; set; }
            public DiskState State { get; set; }

            public int Index => BoardSize * Y + X;

            public Cell(int x, int y, DiskState state = DiskState.Empty)
            {
                X = x;
                Y = y;
                State = state;
            }
        }
    }
}
