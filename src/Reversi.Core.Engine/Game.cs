﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Reversi.Core.Engine.Extensions;

namespace Reversi.Core.Engine
{
    public class Game : IGame
    {
        private readonly IList<string> _transcript = new List<string>();
        private IBoard _board = new Board();

        public Game() { }

        public Game(string transcript) : this()
        {
            if (string.IsNullOrWhiteSpace(transcript))
                return;

            var moves = Utils.TranscriptToMoves(transcript);
            foreach (var move in moves)
            {
                Move(move);
            }
        }

        public DiskState CurrentDisk { get; set; } = DiskState.Black;
        public int MoveNumber { get; private set; } = 1;
        public string Transcript => string.Concat(_transcript);
        public Score Score => _board.Score;
        public bool IsGameOver => !_board.AvailableMoves.Any();

        public IEnumerable<string> AvailableMoves => 
            _board.AvailableMoves.ContainsKey(CurrentDisk) 
            ? _board.AvailableMoves[CurrentDisk].Select(Board.CoordinatesFromIndex) 
            : new List<string>();

        public IEnumerable<DiskState> BoardState => _board;

        public void Move(string coordinates)
        {
            _board = _board.Move(coordinates, CurrentDisk);
            _transcript.Add(coordinates);

            if (_board.AvailableMoves.ContainsKey(CurrentDisk.Flip()))
            {
                CurrentDisk = CurrentDisk.Flip();
            }

            MoveNumber++;
        }

        public async Task MoveAsync(string coordinates, CancellationToken cancellationToken = default(CancellationToken))
        {
            _board = await _board.MoveAsync(coordinates, CurrentDisk, cancellationToken);
            _transcript.Add(coordinates);

            if (_board.AvailableMoves.ContainsKey(CurrentDisk.Flip()))
            {
                CurrentDisk = CurrentDisk.Flip();
            }

            MoveNumber++;
        }
    }
}