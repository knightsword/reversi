﻿using System;

namespace Reversi.Core.Engine
{
    public class InvalidCoordinatesException : Exception
    {
        private const string BaseExceptionMessage = "Coordinates `{0}` do not conform to a location on the board.";

        public string Coordinates { get; set; }

        public InvalidCoordinatesException(string coordinates) : base(string.Format(BaseExceptionMessage, coordinates))
        {
            Coordinates = coordinates;
        }

        public InvalidCoordinatesException(string coordinates, string message) : base(message)
        {
            Coordinates = coordinates;
        }

        public InvalidCoordinatesException(string coordinates, string message, Exception innerException) 
            : base(message, innerException)
        {
            Coordinates = coordinates;
        }
    }
}