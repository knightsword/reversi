﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Reversi.Core.Engine.Extensions
{
    public static class ExtensionMethods
    {
        private const DiskState DiskStateMask = (DiskState) 0x3;

        internal static IEnumerable<T> ExcludeIf<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        {
            return collection.Where(i => !predicate.Invoke(i));
        }

        public static DiskState Flip(this DiskState disk)
        {
            if (disk == DiskState.Empty)
                return disk;

            return ~disk & DiskStateMask;
        }
    }
}
