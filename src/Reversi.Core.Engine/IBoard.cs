﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Core.Engine
{
    public interface IBoard : IEnumerable<DiskState>
    {
        bool HasAnyMoves { get; }
        IDictionary<DiskState, bool> HasMoves { get; }
        bool IsValidMove(string coordinates, DiskState disk);
        Task<bool> IsValidMoveAsync(string coordinates, DiskState disk, CancellationToken cancellationToken = default(CancellationToken));
        bool IsValidMove(int index, DiskState disk);
        Task<bool> IsValidMoveAsync(int index, DiskState disk, CancellationToken cancellationToken = default(CancellationToken));
        IBoard Move(string coordinates, DiskState disk);
        Task<IBoard> MoveAsync(string coordinates, DiskState disk, CancellationToken cancellationToken = default(CancellationToken));
        IBoard Move(char x, int y, DiskState disk);
        Task<IBoard> MoveAsync(char x, int y, DiskState disk, CancellationToken cancellationToken = default(CancellationToken));
        DiskState this[string coordinates] { get; }
        DiskState this[char x, int y] { get; }
        DiskState this[int y, char x] { get; }
        DiskState this[int x, int y] { get; }
        IDictionary<DiskState, IEnumerable<int>> AvailableMoves { get; }
        Score Score { get; }
    }
}