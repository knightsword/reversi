﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Core.Engine
{
    public interface IGame
    {
        DiskState CurrentDisk { get; set; }
        int MoveNumber { get; }
        string Transcript { get; }
        Score Score { get; }
        bool IsGameOver { get; }
        IEnumerable<string> AvailableMoves { get; }
        IEnumerable<DiskState> BoardState { get; }

        void Move(string coordinates);
        Task MoveAsync(string coordinates, CancellationToken cancellationToken = default(CancellationToken));
    }
}