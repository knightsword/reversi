﻿using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace Reversi.Core.Engine
{
    public static class Utils
    {
        public static IEnumerable<string> TranscriptToMoves(string transcript)
        {
            var moves = transcript
                .Segment((c, i) => i % 2 == 0)
                .Select(x => string.Concat(x));

            return moves.ToList();
        }
    }
}
