﻿using System;

namespace Reversi.Core.Engine
{
    public class InvalidMoveException : Exception
    {
        public IBoard Board { get; set; }
        public string MoveCoordinates { get; set; }

        public InvalidMoveException(IBoard board, string move) : base($"Invalid move `{move}` in board.")
        {
            Board = board;
            MoveCoordinates = move;
        }

        public InvalidMoveException(IBoard board, int moveIndex) 
            : this(board, Engine.Board.CoordinatesFromIndex(moveIndex)) { }

        public InvalidMoveException(IBoard board, string move, string message) : base(message)
        {
            Board = board;
            MoveCoordinates = move;
        }

        public InvalidMoveException(IBoard board, int moveIndex, string message) 
            : this(board, Engine.Board.CoordinatesFromIndex(moveIndex), message) { }

        public InvalidMoveException(IBoard board, string move, string message, Exception innerException) 
            : base(message, innerException)
        {
            Board = board;
            MoveCoordinates = move;
        }

        public InvalidMoveException(IBoard board, int moveIndex, string message, Exception innerException) 
            : this(board, Engine.Board.CoordinatesFromIndex(moveIndex), message, innerException) { }
    }
}