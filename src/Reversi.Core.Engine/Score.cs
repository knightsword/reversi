﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Reversi.Core.Engine
{
    public struct Score
    {
        private static readonly IEnumerable<DiskState> ValidDiskStates = new[] { DiskState.Black, DiskState.White };

        private int _black;
        private int _white;

        public int this[DiskState disk]
        {
            get
            {
                switch (disk)
                {
                    case DiskState.Black:
                        return _black;
                    case DiskState.White:
                        return _white;
                    case DiskState.Empty:
                        ThrowForInvalidDiskState(disk);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(disk), disk, null);
                }
                throw new ArgumentOutOfRangeException(nameof(disk), disk, null);
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException(nameof(value), "Score value must be greater than zero.");
                switch (disk)
                {
                    case DiskState.Black:
                        _black = value;
                        break;
                    case DiskState.White:
                        _white = value;
                        break;
                    case DiskState.Empty:
                        ThrowForInvalidDiskState(disk);
                        break;
                    default:
                        ThrowForInvalidDiskState(disk);
                        break;
                }
            }
        }

        private static void ThrowForInvalidDiskState(DiskState disk)
        {
            if (!ValidDiskStates.Contains(disk))
                throw new InvalidDiskStateException(disk);
        }

        public Score(int black = 0, int white = 0)
        {
            _black = black;
            _white = white;
        }

        public override string ToString() => $"{_black}/{_white}";
    }
}