﻿using System.Collections.Generic;
using System.Linq;

namespace Reversi.Core.Engine
{
    public class ScoreComparer : IComparer<Score>
    {
        private static readonly IEnumerable<DiskState> ValidDiskStates = new[] { DiskState.Black, DiskState.White };
        private readonly DiskState _comparerPivot;

        protected ScoreComparer(DiskState comparerPivot)
        {
            ThrowForInvalidDiskState(comparerPivot);
            _comparerPivot = comparerPivot;
        }

        private static void ThrowForInvalidDiskState(DiskState disk)
        {
            if (!ValidDiskStates.Contains(disk))
                throw new InvalidDiskStateException(disk);
        }

        public static IComparer<Score> BlackDisk => new ScoreComparer(DiskState.Black);
        public static IComparer<Score> WhiteDisk => new ScoreComparer(DiskState.White);

        public static IDictionary<DiskState, IComparer<Score>> For => 
            new Dictionary<DiskState, IComparer<Score>>
            {
                { DiskState.Black, BlackDisk },
                { DiskState.White, WhiteDisk }
            };

        public int Compare(Score x, Score y) => x[_comparerPivot].CompareTo(y[_comparerPivot]);
    }
}