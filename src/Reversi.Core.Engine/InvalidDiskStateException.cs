﻿using System;

namespace Reversi.Core.Engine
{
    public class InvalidDiskStateException : Exception
    {
        public DiskState Disk { get; }
        public InvalidDiskStateException(DiskState disk)
            : base($"Disk state `{disk}` cannot be used in this context.")
        {
            Disk = disk;
        }

        public InvalidDiskStateException(DiskState disk, string message) : base(message)
        {
            Disk = disk;
        }

        public InvalidDiskStateException(DiskState disk, string message, Exception innerException) 
            : base(message, innerException)
        {
            Disk = disk;
        }
    }
}