﻿using System;

namespace Reversi.Core.Engine
{
    [Flags]
    public enum DiskState
    {
        Empty = 0x0,
        Black = 0x1,
        White = 0x2
    }
}
