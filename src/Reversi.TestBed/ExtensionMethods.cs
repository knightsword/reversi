﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using Reversi.AI;
using Reversi.Core.Engine;

namespace Reversi.TestBed
{
    public static class ExtensionMethods
    {
        public static string ToString(this IEnumerable<DiskState> collection)
        {
            var diskStates = collection.ToList();
            var builder = new StringBuilder();
            while (diskStates.Any())
            {
                var group = diskStates.Take(8);
                diskStates = diskStates.Skip(8).ToList();
                var line = string.Concat(group.Select(d => d.ToString("G").First()));
                line = line.Replace("E", " ");
                builder.AppendLine(line);
            }

            return builder.ToString();
        }

        public static TagBuilder ToTreeList(this MoveTree moveTree, int depth = 10)
        {
            var divBuilder = new TagBuilder("div");
            var item = $"Disk={moveTree.Disk}, Score={moveTree.Board.Score.ToString()}";
            divBuilder.InnerHtml.AppendHtml(item);

            if (depth < 0)
                return divBuilder;

            var listBuilder = new TagBuilder("ul");
            foreach (var node in moveTree.MoveStates)
            {
                var itemBuilder = new TagBuilder("li");
                itemBuilder.InnerHtml.AppendHtml($"<div><strong>{node.Move}</strong></div>");
                itemBuilder.InnerHtml.AppendHtml(node.MoveState.ToTreeList(depth - 1));
                listBuilder.InnerHtml.AppendHtml(itemBuilder);
            }
            divBuilder.InnerHtml.AppendHtml(listBuilder);

            return divBuilder;
        }
    }
}
