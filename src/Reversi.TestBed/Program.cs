﻿using System;
using System.Linq;
using MoreLinq.Extensions;
using Reversi.AI;
using Reversi.Core.Engine;

namespace Reversi.TestBed
{
    internal class Program
    {
        private static readonly BotConfiguration WhiteConfig = new BotConfiguration { MaxSearchDepth = 5 };
        private static readonly BotConfiguration BlackConfig = new BotConfiguration { Disk = DiskState.Black, MaxSearchDepth = 0};
        private static void Main(string[] args)
        {
            RandomGameTest();
            //GameTranscriptTest();
            //MoveTreeBreakdownTest(args);
            ReversiBotTest();

            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        //private static void MoveTreeBreakdownTest(string[] args)
        //{
        //    var depth = 10;
        //    var parsed = args.Any() && int.TryParse(args[0], out depth);
        //    depth = parsed ? depth : 9;

        //    if (!args.Contains("-o"))
        //        return;

        //    var argIndex = Array.IndexOf(args, "-o");
        //    var outputFilename = args[argIndex + 1];
        //    BoardMoveBreakDownTest(outputFilename, depth);
        //}

        //private static void BoardMoveBreakDownTest(string outputFilename, int depth = 10)
        //{
        //    var game = new Game();
        //    var blackBot = new ReversiBot(BlackConfig);
        //    var moveTree = blackBot.GetMoveTree(game).ToTreeList(depth);

        //    var html = new TagBuilder("html");
        //    var head = new TagBuilder("head");
        //    head.InnerHtml.AppendHtml("<title>Test Tree</title>");
        //    var body = new TagBuilder("body");
        //    body.InnerHtml.AppendHtml(moveTree);
        //    html.InnerHtml.AppendHtml(head);
        //    html.InnerHtml.AppendHtml(body);

        //    using (var output = File.Open(outputFilename, FileMode.Create, FileAccess.Write, FileShare.None))
        //    using (var writer = new StreamWriter(output))
        //    {
        //        html.WriteTo(writer, HtmlEncoder.Default);
        //    }
        //}

        private static void ReversiBotTest()
        {
            var blackBot = new ReversiBot(BlackConfig);
            var whiteBot = new ReversiBot(WhiteConfig);
            var game = new Game("D3E3F4G3F3C5H3F2C4C3E2E1B3H4");

            var blackMove = blackBot.GetMove(game);
            game.Move(blackMove);

            var whiteMove = whiteBot.GetMove(game);
            game.Move(whiteMove);

        }

        private static void GameTranscriptTest()
        {
            //var game = new Game("F5D6C5F6C4F4E6D7E7C6F7D8C8E8G5B8E3F8B6B5A6A4A5A7C7B4G6H6H7B3D3C3C2C1H5D2E2F1B1D1B2A3E1H4H3G4G3F3G1F2B7H2H1G7");
            var game = new Game("D3E3F4G3F3C5H3F2C4C3E2E1B3H4H5A3");

            Console.WriteLine("- Board State -");
            Console.WriteLine(ExtensionMethods.ToString(game.BoardState));
        }

        private static void RandomGameTest()
        {
            var game = new Game();

            while (!game.IsGameOver)
            {
                var move = game.AvailableMoves.Shuffle().First();
                game.Move(move);
            }

            Console.WriteLine("- FINAL SCORE -");
            Console.WriteLine("===============");
            Console.WriteLine("Black - {0}", game.Score[DiskState.Black]);
            Console.WriteLine("White - {0}", game.Score[DiskState.White]);
            Console.WriteLine();
            Console.WriteLine("Transcript - {0}", game.Transcript);
            Console.WriteLine();
            Console.WriteLine("- Board State -");
            Console.Write(ExtensionMethods.ToString(game.BoardState));
            Console.WriteLine();
            Console.WriteLine("===============");
        }
    }
}
