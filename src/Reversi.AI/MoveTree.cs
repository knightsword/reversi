﻿using System.Collections.Generic;
using System.Linq;
using Reversi.Core.Engine;
using Reversi.Core.Engine.Extensions;

namespace Reversi.AI
{
    public class MoveTree
    {
        public DiskState Disk { get; }
        public IBoard Board { get; }

        public IEnumerable<MoveNode> MoveStates
        {
            get
            {
                if (!Board.HasMoves[Disk])
                    return Enumerable.Empty<MoveNode>();

                var nodes = Board
                    .AvailableMoves[Disk]
                    .Select(m => new MoveNode
                    {
                        Move = Core.Engine.Board.CoordinatesFromIndex(m),
                        MoveState = new MoveTree(Disk.Flip(), Board.Move(Core.Engine.Board.CoordinatesFromIndex(m), Disk))
                    });

                return nodes;
            }
        }

        public IEnumerable<BoardMoveScore> MaxBoardMoveScores
        {
            get
            {
                var maxScore = MoveStates
                    .OrderByDescending(s => s.MoveState.Board.Score, ScoreComparer.For[Disk])
                    .FirstOrDefault()
                    ?.MoveState
                    ?.Board
                    ?.Score ?? new Score();

                return MoveStates
                    .Where(n => ScoreComparer.For[Disk].Compare(n.MoveState.Board.Score, maxScore) == 0)
                    .Select(n => n.ToBoardMoveScore(Disk));
            }
        }

        public IEnumerable<BoardMoveScore> MinBoardMoveScores
        {
            get
            {
                var minScore = MoveStates
                    .OrderBy(s => s.MoveState.Board.Score, ScoreComparer.For[Disk])
                    .FirstOrDefault()
                    ?.MoveState
                    ?.Board
                    ?.Score ?? new Score();

                return MoveStates
                    .Where(n => ScoreComparer.For[Disk].Compare(n.MoveState.Board.Score, minScore) == 0)
                    .Select(n => n.ToBoardMoveScore(Disk));
            }
        }

        public MoveTree(DiskState disk, IBoard board)
        {
            Board = board;

            Disk = Board.AvailableMoves.ContainsKey(disk) 
                ? disk 
                : disk.Flip();
        }

        public class MoveNode
        {
            public string Move { get; set; }
            public MoveTree MoveState { get; set; }

            public BoardMoveScore ToBoardMoveScore(DiskState disk) => 
                new BoardMoveScore(Move, MoveState.Board.Score, !MoveState.Board.HasAnyMoves);
        }
    }
}