﻿using Reversi.Core.Engine;

namespace Reversi.AI
{
    public class BotConfiguration
    {
        public DiskState Disk { get; set; } = DiskState.White;

        public int MaxSearchDepth { get; set; } = 10;
    }
}