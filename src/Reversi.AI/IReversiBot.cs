﻿using System.Threading;
using System.Threading.Tasks;
using Reversi.Core.Engine;

namespace Reversi.AI
{
    public interface IReversiBot
    {
        DiskState Disk { get; }

        string GetMove(DiskState disk, IBoard board);
        Task<string> GetMoveAsync(DiskState disk, IBoard board, CancellationToken cancellationToken = default(CancellationToken));
        string GetMove(IGame game);
        Task<string> GetMoveAsync(IGame game, CancellationToken cancellationToken = default(CancellationToken));
    }
}