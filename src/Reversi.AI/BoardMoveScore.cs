﻿using System.Diagnostics;
using Reversi.Core.Engine;

namespace Reversi.AI
{
    [DebuggerDisplay("Move={Move}, Score={Score[Reversi.Core.Engine.DiskState.Black]}/{Score[Reversi.Core.Engine.DiskState.White]}, IsOver={IsOver}")]
    public struct BoardMoveScore
    {
        public string Move { get; }
        public Score Score { get; }
        public bool IsOver { get; }

        public BoardMoveScore(string move, Score score, bool isOver)
        {
            Move = move;
            Score = score;
            IsOver = isOver;
        }
    }
}
