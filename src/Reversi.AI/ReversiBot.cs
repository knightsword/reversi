﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MoreLinq;
using Reversi.Core.Engine;

namespace Reversi.AI
{
    public class ReversiBot : IReversiBot
    {
        private readonly BotConfiguration _configuration;

        public ReversiBot(BotConfiguration configuration)
        {
            _configuration = configuration;
        }

        public DiskState Disk => _configuration.Disk;

        public string GetMove(DiskState disk, IBoard board)
        {
            var moveTree = GetMoveTree(disk, board);
            var moves = Analyze(moveTree);
            var move = moves
                .Shuffle()
                .First();
            return move;
        }

        public async Task<string> GetMoveAsync(DiskState disk, IBoard board, CancellationToken cancellationToken = default(CancellationToken))
        {
            var moveTree = await GetMoveTreeAsync(disk, board, cancellationToken);
            var moves = Analyze(moveTree);
            var move = moves
                .Shuffle()
                .First();
            return move;
        }

        public string GetMove(IGame game)
        {
            var board = new Board(game.BoardState);
            return GetMove(game.CurrentDisk, board);
        }

        public async Task<string> GetMoveAsync(IGame game, CancellationToken cancellationToken = default(CancellationToken))
        {
            var board = new Board(game.BoardState);
            return await GetMoveAsync(game.CurrentDisk, board, cancellationToken);
        }

        private IEnumerable<string> Analyze(MoveTree moveTree)
        {
            var boardStates = ComputeBoardBestMoves(moveTree, _configuration.MaxSearchDepth).ToList();
            var bestMoves = CollectBestMoves(boardStates, moveTree.Disk);
            return bestMoves.Select(s => s.Move);
        }

        //private async Task<IEnumerable<string>> AnalyzeAsync(MoveTree moveTree, CancellationToken cancellationToken = default(CancellationToken))
        //{
        //    var boardStates = (await ComputeBoardBestMovesAsync(moveTree, _configuration.MaxSearchDepth)).ToList();
        //    var bestMoves = await CollectBestMovesAsync(boardStates, moveTree.Disk);
        //    return bestMoves.Select(s => s.Move);
        //}

        private static IEnumerable<BoardMoveScore> CollectBestMoves(IEnumerable<BoardMoveScore> boardMoveScores, DiskState disk)
        {
            var moveScores = boardMoveScores.ToList();
            var primeScore = moveScores
                .OrderByDescending(s => s.IsOver)
                .ThenByDescending(s => s.Score, ScoreComparer.For[disk])
                .FirstOrDefault()
                .Score;

            return moveScores
                .AsParallel()
                .Where(s => s.Score.Equals(primeScore));
        }

        private IEnumerable<BoardMoveScore> ComputeBoardBestMoves(MoveTree moveTree, int depth)
        {
            if (depth == 0)
            {
                return _configuration.Disk == moveTree.Disk 
                    ? moveTree.MaxBoardMoveScores 
                    : moveTree.MinBoardMoveScores;
            }

            if (!moveTree.MoveStates.Any())
                return Enumerable.Empty<BoardMoveScore>();

            if (_configuration.Disk == moveTree.Disk)
            {
                // max move collect
                var nodeScores = moveTree
                    .MoveStates
                    .Select(n =>
                {
                    var childScores = ComputeBoardBestMoves(n.MoveState, depth - 1).ToList();

                    if (!childScores.Any())
                        return n.ToBoardMoveScore(moveTree.Disk);

                    var maxScore = childScores
                        .OrderByDescending(s => s.Score, ScoreComparer.For[_configuration.Disk])
                        .First()
                        .Score;

                    return new BoardMoveScore(n.Move, maxScore, !moveTree.Board.HasAnyMoves);
                });
                return nodeScores;
            }
            else
            {
                // min move collect
                var nodeScores = moveTree
                    .MoveStates
                    .Select(n =>
                {
                    var childScores = ComputeBoardBestMoves(n.MoveState, depth - 1).ToList();

                    if (!childScores.Any())
                        return n.ToBoardMoveScore(moveTree.Disk);
                        
                    var minScore = childScores
                        .OrderBy(s => s.Score, ScoreComparer.For[_configuration.Disk])
                        .First()
                        .Score;

                    return new BoardMoveScore(n.Move, minScore, !moveTree.Board.HasAnyMoves);
                });
                return nodeScores;
            }
        }

        private async Task<IEnumerable<BoardMoveScore>> ComputeBoardBestMovesAsync(MoveTree moveTree, int depth, CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        private static MoveTree GetMoveTree(DiskState disk, IBoard board) => new MoveTree(disk, board);

        private static Task<MoveTree> GetMoveTreeAsync(DiskState disk, IBoard board, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            var moveTree = new MoveTree(disk, board);
            return Task.FromResult(moveTree);
        }
    }
}
