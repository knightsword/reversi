﻿using System;

namespace Reversi.AI
{
    public class InvalidDiskStateException : Exception
    {
        public InvalidDiskStateException() : base("Object has an invalid disk state") { }

        public InvalidDiskStateException(string message) : base(message) { }

        public InvalidDiskStateException(string message, Exception innerException) : base(message, innerException) { }
    }
}