﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Reversi.AI;
using Reversi.Core.Engine;

namespace Reversi.Web.UI.Modules
{
    public class ApplicationModule : Module
    {
        private readonly IConfiguration _config;

        public ApplicationModule(IConfiguration config)
        {
            _config = config;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new BotConfiguration
            {
                Disk = DiskState.White,
                MaxSearchDepth = 5
            })
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<ReversiBot>()
                .As<IReversiBot>()
                .SingleInstance();
        }
    }
}
