﻿using Reversi.Core.Engine;

namespace Reversi.Web.UI.Models
{
    public class CellViewModel
    {
        public Game Game { get; set; }
        public DiskState Disk { get; set; }
        public bool IsMove { get; set; }
        public string Coordinates { get; set; }
    }
}
