﻿namespace Reversi.Web.UI.Models
{
    public class MoveViewModel
    {
        public string Transcript { get; set; }
        public string Move { get; set; }
    }
}