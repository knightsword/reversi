﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reversi.AI;
using Reversi.Core.Engine;
using Reversi.Web.UI.Models;

namespace Reversi.Web.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IReversiBot _reversiBot;

        public HomeController(IReversiBot reversiBot)
        {
            _reversiBot = reversiBot;
        }

        [HttpGet("{transcript?}")]
        public IActionResult Index(string transcript)
        {
            var viewModel = new GameViewModel
            {
                Transcript = transcript
            };
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Move(MoveViewModel viewModel)
        {
            var game = new Game(viewModel.Transcript);
            await game.MoveAsync(viewModel.Move);

            if (game.CurrentDisk != _reversiBot.Disk)
                return RedirectToAction("Index", "Home", new {transcript = game.Transcript});

            var botMove = await _reversiBot.GetMoveAsync(game);
            game.Move(botMove);

            return RedirectToAction("Index", "Home", new {transcript = game.Transcript});
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
